# GCS Email alarm visualization

Web application based on FastAPI for backend + Tabulator and Plotly and Tailwindcss for frontend visualization of the alarms received from the 
gas control system.


# Local development

First, create a .env file with the `DSN` to connect to the postgresql db:
```sh
echo 'DSN=...' > .env
```

Run the project locally: use two terminals, one for tailwindcss:
```sh
tailwindcss -i app/styles/main.css -o app/static/css/main.css --watch
```

And one for the web server using `uvicorn`:
```sh
uvicorn app.main:app --reload
```


## Run it on Docker

To run it on docker, build the image first


```sh
docker build -t gcs-email-alarms-visualization .
```

And run it, passing the `DSN` env variable for the connection to the postgresql database.
If you want to run it on a custom port like 5000 do the following:
```sh
docker run -p 5000:80 -e  DSN="host=... user=... password=... port=... dbname=... sslmode=disable" gcs-email-alarms-visualization:latest
```
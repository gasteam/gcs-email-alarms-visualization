from fastapi import FastAPI, Request, Depends, Query, Form, Body
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic_settings import BaseSettings, SettingsConfigDict
import psycopg2
from psycopg2 import sql
from psycopg2.extras import RealDictCursor
from typing import Annotated
from datetime import date, datetime
import pandas as pd
from functools import lru_cache
import time
import plotly.express as px
import plotly


class Settings(BaseSettings):
    DSN: str

    model_config = SettingsConfigDict(env_file=".env")


async def get_settings():
    yield Settings()


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


async def get_db(settings: Annotated[Settings, Depends(get_settings)]):
    conn = psycopg2.connect(settings.DSN)
    try:
        yield conn
    finally:
        conn.close()


templates = Jinja2Templates(directory="app/templates")
app = FastAPI()
app.mount("/static", StaticFiles(directory="app/static"), name="static")


@app.get("/alarm")
async def alarm(
    request: Request,
    id: Annotated[str, Query()],
    db: psycopg2.extensions.connection = Depends(get_db),
):
    with db.cursor(cursor_factory=RealDictCursor) as cursor:
        cursor.execute(
            "SELECT * FROM schema_emails_parsed.table_emails_parsed WHERE id = %(id)s",
            {"id": id},
        )
        alarm = cursor.fetchone()

    return templates.TemplateResponse(
        "alarm.html", context={"request": request, "alarm": alarm}
    )


@app.post("/alarms")
async def alarms(
    request: Request,
    page: Annotated[int | None, Body(ge=1)] = 1,
    size: Annotated[int | None, Body(lt=1000)] = 100,
    filter: Annotated[list[dict[str, str | bool]] | None, Body()] = None,
    sorters: dict = None,
    db: psycopg2.extensions.connection = Depends(get_db),
):
    offset = (page - 1) * size
    n_pages = 0

    with db.cursor(cursor_factory=RealDictCursor) as cursor:
        # Get the number of records to understand how many pages are required
        cursor.execute(
            """
            -- from https://stackoverflow.com/questions/7943233/fast-way-to-discover-the-row-count-of-a-table-in-postgresql
            SELECT c.reltuples::bigint AS estimate
            FROM   pg_class c
            JOIN   pg_namespace n ON n.oid = c.relnamespace
            WHERE  c.relname = 'table_emails_parsed'
            AND    n.nspname = 'schema_emails_parsed';
        """
        )
        result = cursor.fetchone()
        n_rows = result["estimate"]
        n_pages = n_rows // size if n_rows % size == 0 else n_rows // size + 1

        filter_query = sql.SQL("")

        where_clauses = []
        values = []
        for filter_obj in filter:
            if filter_obj["field"] == "alarm_timestamp":
                field = sql.Identifier(f"{filter_obj['field']}") + sql.SQL("::date")
            else:
                field = sql.Identifier(filter_obj["field"])

            type = filter_obj["type"]
            value = filter_obj["value"]
            if isinstance(type, str) and type.upper() == "LIKE":
                type = "ILIKE"
                value = f"%{value}%"

            # Construct the WHERE clause and append it to the list
            where_clause = sql.SQL("{} {} %s").format(field, sql.SQL(type))
            where_clauses.append(where_clause)
            values.append(value)

        # Join the WHERE clauses using the AND operator
        joined_clauses = sql.SQL(" AND ").join(where_clauses)
        if filter:
            where_clause = sql.SQL(" WHERE {}").format(joined_clauses)
        else:
            where_clause = sql.SQL(" ")

        main_query = sql.SQL(
            """
            SELECT *
            FROM (
                SELECT 
                    id, experiment, system, alarm_name, alarm_status, 
                    alarm_timestamp, alarm_text, category, is_alarm, during_working_hours,
                    extract('isoyear' from alarm_timestamp) as year,
                    extract('week' from alarm_timestamp) as week 
                FROM schema_emails_parsed.table_emails_parsed
            ) AS subquery
            {where_clause}
            order by alarm_timestamp desc, id  desc
            limit %s offset %s
        """
        ).format(where_clause=where_clause)

        cursor.execute(main_query, [*values, size, offset])

        alarms = cursor.fetchall()

    return JSONResponse(
        content={"last_page": n_pages, "data": jsonable_encoder(alarms)}
    )


@lru_cache
def make_plots(db: psycopg2.extensions.connection, ttl_hash=None):
    del ttl_hash  # used only by lru_cache
    df = pd.read_sql(
        """
        SELECT extract('isoyear' from alarm_timestamp) || 'W' || extract('week' from alarm_timestamp) as year_week,
        during_working_hours,
        min(extract('isoyear' from alarm_timestamp)) as year,
        min(extract('week' from alarm_timestamp)) as week,
        count(*)
        FROM schema_emails_parsed.table_emails_parsed 
        WHERE is_alarm = true
        group by extract('isoyear' from alarm_timestamp) || 'W' || extract('week' from alarm_timestamp), during_working_hours
        order by min(extract('isoyear' from alarm_timestamp)), min(extract('week' from alarm_timestamp))""",
        db,
    )
    rows = len(df.year_week.unique())

    fig = px.bar(
        df, x="year_week", y="count", color="during_working_hours", barmode="group"
    )
    fig.update_layout(yaxis_range=[0, 100], title_text="Alarms received")
    fig.update_xaxes(
        type="category",
        categoryorder="array",
        categoryarray=df.year_week.tolist(),
        title_text="Year and Week",
        range=[rows - 40, rows],
    )
    fig.update_yaxes(title_text="Number of alarms")

    return plotly.io.to_json(fig)


def get_ttl_hash(seconds=3600):
    """Return the same value withing `seconds` time period"""
    return round(time.time() / seconds)


@app.get("/")
async def home(request: Request, db: psycopg2.extensions.connection = Depends(get_db)):
    # Try to generate a plot
    json_plot = make_plots(db, get_ttl_hash())
    context = {
        "request": request,
        "plot_stats_alarms": json_plot,
    }
    return templates.TemplateResponse("main.html", context)
